;;; good-mode.el --- Edit text efficiently  -*- lexical-binding: t -*-

;; Copyright (C) 2022 Free Software Foundation, Inc.
;; Author: q01@disroot.org
;; Maintainer: q01@disroot.org
;; Keywords: convenience
;; Created: Oct 17, 2022
;; Version: 0.5
;; URL: https://gitlab.com/q01_code/emacs-good-mode

;; This file is NOT part of GNU Emacs.

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:
;;
;; This package aims to extend and simplify Emacs' text editing capabilities.
;;
;; To activate it globally, type M-x good-mode RET
;; 
;; You can also enable it by putting the following lines at your init file:
;; 
;; (require 'good-mode)
;; (good-mode)
;; 
;; By default, this package defines two new special transient states which
;; allow editing using simple, mnemonic commands. The default bindings for
;; those are C-k and M-k, which then activates `good-mode--kill-mode' and
;; `good-mode--mark-mode', respectively.
;;
;; Alternatively, good-mode can:
;; - Define two new prefix for all killing and marking operations, or
;; - Define two new commands for killing and marking text which do
;;   different things depending on situation.

;;; Code:

(require 'cl-lib)
(require 'thingatpt)

;; Variables

(defgroup good-mode nil
  "Extending and simplifying Emacs' text editing capabilities."
  :group 'editing)

(defcustom good-mode--use-transient-mode t
  "If non-nil, enter a transient state when doing some operations.
This option is only respected by good-mode--kill-command and
good-mode--mark-command, the only commands compatible with such
transient state.
Note that both default and DWIM approaches can use the aforementioned
commands.
Setting this to nil will basically break numeric argument support.
It is recommended to use the prefix approach instead of setting this variable."
  :group 'good-mode
  :type 'boolean)

(defcustom good-mode--remove-spaces-after-killing nil
  "Wheter remove whitespace after some killing operations.
It is disabled by default because it doesn't work reliably
(specially on repetitions), but it's technically usable.
It will be set to t in a future version."
  :group 'good-mode
  :type 'boolean)

(defcustom good-mode--kill-mode-message "[Kill mode]"
  "String displayed when entering good-mode--kill-command."
  :group 'good-mode
  :type 'string)

(defcustom good-mode--mark-mode-message "[Mark mode]"
  "String displayed when entering good-mode--mark-command."
  :group 'good-mode
  :type 'string)

(defcustom good-mode--transient-timeout nil
  "Amount of seconds after which transient-mode will be automatically disabled.
If nil, don't disable the mode automatically.
It won't work on Emacs version < 29."
  :group 'good-mode
  :type 'integer)

;; Helpers

(defun good-echo (string &rest args)
  "Display an unlogged message in the echo area.
That is, the message is not logged in the *Messages* buffer."
  (let (message-log-max)
    (apply #'message string args)))

(defun good-mode--remove-hooks ()
  "Remove hooks created by good-mode."
  (remove-hook 'pre-command-hook 'good-mode--echo-mark)
  (remove-hook 'pre-command-hook 'good-mode--echo-kill)
  (message ""))

(defun good-mode--echo-kill ()
  "Echo a message if necessary."
  (if overriding-terminal-local-map
      (good-echo good-mode--kill-mode-message)))

(defun good-mode--echo-mark ()
  "Echo a message if necessary."
  (if overriding-terminal-local-map
      (good-echo good-mode--mark-mode-message)))

(defun gm/change-direction-p (arg)
  "Return t if it seems convenient to move point before next command."
  (if (region-active-p)
    (if (>= arg 0)
        (unless (<= (point) (mark)) t)
      (unless (>= (point) (mark)) t))
    nil))

(defun gm/kill-thing (thing &optional arg no-move)
  "Kill THING at point. If there's no such thing, signal an error.
The entire THING is killed, regardless of point position.
With ARG, kill from beginning of THING to end of N-1 THINGS.
With negative ARG, kill from end of THING to beginning of N+1
THINGS looking backwards.
If optional NO-MOVE is non-nil, don't kill the whole thing.
For example, if in the middle of a word, and THING equals to word,
just kill until the end of word."
  (let ((bounds (bounds-of-thing-at-point thing)))
    (or bounds
        (error "No %s at point" thing))
    (or arg (setq arg 0))
    (if no-move
        (kill-region
         (point)
         (save-excursion
           (forward-thing thing arg)
           (point)))
      (kill-region (car bounds) (cdr bounds)))
    (append-next-kill)
    (unless (or no-move (eq arg 0))
      (kill-region
       (point)
       (save-excursion
         (if (< 0 arg)
             (forward-thing thing (- arg 1))
           (forward-thing thing (+ arg 1)))
         (point))))))

(defun gm/mark-thing (thing &optional arg no-move)
  "Put point at beginning of THING, mark at end.
Interactively, if the mark is active, it marks the
next ARG things after the ones already marked.
If optional NO-MOVE is non-nil, don't move point.
The code is based on mark-word definition."
  (let ((bounds (bounds-of-thing-at-point thing)))
    (or bounds
        (error "No %s at point" thing))
    (or arg (setq arg 1))
    (unless (or (gm/change-direction-p arg) no-move)
      (if (>= arg 0)
          (goto-char (car bounds))
        (goto-char (cdr bounds))))
    (cond ((region-active-p)
	   (setq arg
                 (if (= arg 1)
	             (if (< (mark) (point)) -1 1)
                   (prefix-numeric-value arg)))
	   (set-mark
	    (save-excursion
	      (goto-char (mark))
	      (forward-thing thing arg)
	      (point))))
	  (t
	   (push-mark
	    (save-excursion
	      (forward-thing thing (prefix-numeric-value arg))
	      (point))
	    nil t)
           (or transient-mark-mode (activate-mark))))))

(defun gm/mark-region (beg end)
  "Put point at BEG, mark at END."
  (goto-char beg)
  (push-mark end nil t))

(defun gm/just-one-space-post-kill-operation (&rest _)
  "Function to manage white space after some `kill-whole-thing' operations."
  (save-excursion
    (cond ((looking-back "^ *" 2) ; remove extra space at beginning of line
           (just-one-space 0)
           (indent-according-to-mode))
           ((or (looking-at   " ")
                (looking-back " " 1)) ; adjust space only if it exists
            (just-one-space 1))
           (t ; do nothing otherwise, includes case where the point is at EOL
            ))))

;; Text object support
;;; This come from the textobjects.el package, by Alessandro Arzilli.
;;; I modernized it, because original code just doesn't work anymore
;;; (it is from 2010!)

(defun gm/pseudo-motion-paren-ex (searched related motion-fn limit)
  (cl-block fn
    (let ((depth-count 0)
          (case-fold-search nil)
          (original-char (point)))
      (while (>= depth-count 0)
        (when (= (point) limit)
          (cl-return-from fn nil))
        (funcall motion-fn)
        (when (eq (char-after) nil)
          (goto-char original-char)
          (error "Couldn't find anything to do here"))
        (cond
         ((char-equal (char-after) related)
          (cl-incf depth-count))
         ((char-equal (char-after) searched)
          (cl-decf depth-count))))
      (cl-return-from fn t))))

(defun gm/get-paren-delimited-region (open close)
  "Return a cons with the values of OPEN and CLOSE chars."
  (let ((current-point (point)) (start-point 0) (end-point 0))
    (unless (gm/pseudo-motion-paren-ex open close 'backward-char (point-min))
      (goto-char current-point)
      (error "No expression found"))
    (forward-char)
    (setq start-point (point))
    (goto-char current-point)
    (unless (gm/pseudo-motion-paren-ex close open 'forward-char (point-max))
      (goto-char current-point)
      (error "No expression found"))
    (setq end-point (point))
    (goto-char current-point)
    (cons start-point end-point)))

(defun gm/pseudo-motion-an-extenders (kind)
  (let ((case-fold-search nil)
        (paren-info (gm/pseudo-motion-get-paren-info kind)))
    (cond
     ((or (char-equal kind ?W) (char-equal kind ?w)) '(?\s . ?\s))
     ((consp paren-info) paren-info)
     ((gm/pseudo-motion-is-quoted-string-p kind) (cons kind kind))
     (t (error "Unknown pseudo motion")))))

(defun gm/pseudo-motion-extend-region-an (kind region)
  (let ((an-extenders (gm/pseudo-motion-an-extenders kind)))
    (when (char-equal (char-before (car region)) (car an-extenders))
      (cl-decf (car region)))
    (when (char-equal (char-after (cdr region)) (cdr an-extenders))
      (cl-incf (cdr region)))
    region))

(defun gm/get-regex-delimited-region (regex noerror)
  (let ((current-point (point)) (start-point 0) (end-point 0))
    (unless (re-search-backward regex (line-beginning-position) noerror)
      (goto-char current-point)
      (error "No expression found"))
    (forward-char)
    (setq start-point (point))
    (goto-char current-point)
    (unless (re-search-forward regex (line-end-position) noerror)
      (goto-char current-point)
      (error "No expression found"))
    (backward-char)
    (setq end-point (point))
    (goto-char current-point)
    (cons start-point end-point)))

(defun gm/pseudo-motion-get-paren-info (char)
  (let ((paren-info-assoc '(
                            (?{ . (?{ . ?}))
                            (?} . (?{ . ?}))
                            ;; (?B . (?{ . ?}))
                            (?\[ . (?\[ . ?\]))
                            (?\] . (?\[ . \]))
                            (?\( . (?\( . ?\)))
                            (?\) . (?\( . ?\)))
                            ;; (?b . (?\( . ?\)))
                            (?< . (?< . ?>))
                            (?> . (?< . ?>))
                            ))
        (case-fold-search nil))
    (assoc-default char paren-info-assoc 'char-equal)))

(defun gm/pseudo-motion-is-quoted-string-p (kind)
  (let ((case-fold-search nil))
    (or (char-equal kind ?\")
        (char-equal kind ?\')
        (char-equal kind ?\`))))

(defun gm/get-pseudo-motion-region (kind)
  (let ((case-fold-search nil)
        (paren-info (gm/pseudo-motion-get-paren-info kind)))
    (let ((region
           (cond
            ((consp paren-info)
             (gm/get-paren-delimited-region (car paren-info) (cdr paren-info)))
            ((gm/pseudo-motion-is-quoted-string-p kind)
             (gm/get-regex-delimited-region (string kind) t))
            (t (error "Unknown pseudo motion")))))
      region)))

(defun gm/pseudo-motion-execute-command-on-region (com region)
  (funcall com (car region) (cdr region)))

(defun gm/pseudo-motion-interactive-base-in (com kind)
  (gm/pseudo-motion-execute-command-on-region com (gm/get-pseudo-motion-region kind)))

(defun gm/pseudo-motion-interactive-base-an (com kind)
  (gm/pseudo-motion-execute-command-on-region com (gm/pseudo-motion-extend-region-an kind (gm/get-pseudo-motion-region kind))))


;; Kill functions

(defun gm/kill-logical-line (&optional arg)
  "Just call `kill-line' with argument ARG."
  (interactive "p")
  (when (eq arg 1) (setq arg nil))
  (kill-line arg))

(defun gm/backward-kill-logical-line ()
  "Kill back from point to start of logical line."
  (interactive)
  (gm/kill-thing 'line 0 t))

(defun gm/kill-whole-paragraph (&optional arg)
  "Kill paragraph at point.
With ARG, kill from beginning of paragraph to end of N-1 paragraphs.
With negative ARG, kill from end of paragraph to beginning of N+1
paragraphs looking backwards."
  (interactive "p")
  (gm/kill-thing 'paragraph arg))

(defun gm/kill-whole-sentence (&optional arg)
  "Kill sentence at point.
With ARG, kill from beginning of sentence at point to end of N-1 sentences.
With negative ARG, kill from end of sentence to beginning of N+1 sentences
looking backwards."
  (interactive "p")
  (gm/kill-thing 'sentence arg)
  (when good-mode--remove-spaces-after-killing (gm/just-one-space-post-kill-operation)))

(defun gm/kill-whole-sexp (&optional arg)
  "Kill sexp at point.
With ARG, kill from beginning of sexp at point to end of N-1 sexps.
With negative ARG, kill from end of sexp to beginning of N+1 sexps
looking backwards.
Beware that it fails if you try to kill sexps outside the scope of current one,
just like `kill-sexp'."
  (interactive "p")
  (gm/kill-thing 'sexp arg))

(defun gm/kill-whole-word (&optional arg)
  "Kill word at point.
With ARG, kill from beginning of word at point to end of N-1 words.
With negative ARG, kill from end of word to beginning of N+1 words
looking backwards."
  (interactive "p")
  (gm/kill-thing 'word arg)
  (when good-mode--remove-spaces-after-killing (gm/just-one-space-post-kill-operation)))

(defun gm/kill-whole-buffer ()
  "Kill the entire (accesible) buffer contents."
  (interactive)
  (kill-region (point-min) (point-max)))

(defun gm/kill-between-brackets (&optional arg)
  "Kill text between brackets.
With ARG, include the brackets"
  (interactive "P")
  (if arg
      (gm/pseudo-motion-interactive-base-an 'kill-region ?\()
    (gm/pseudo-motion-interactive-base-in 'kill-region ?\()))

(defun gm/kill-between-square-brackets (&optional arg)
  "Kill text between square brackets.
With ARG, include the square brackets."
  (interactive "P")
  (if arg
      (gm/pseudo-motion-interactive-base-an 'kill-region ?\[)
    (gm/pseudo-motion-interactive-base-in 'kill-region ?\[)))

(defun gm/kill-between-curly-brackets (&optional arg)
  "Kill text between curly brackets.
With ARG, include the curly brackets."
  (interactive "P")
  (if arg
      (gm/pseudo-motion-interactive-base-an 'kill-region ?\{)
    (gm/pseudo-motion-interactive-base-in 'kill-region ?\{)))

(defun gm/kill-string (&optional arg)
  "Kill a string.
With ARG, include the quotes."
  (interactive "P")
  (if arg
      (gm/pseudo-motion-interactive-base-an 'kill-region ?\")
    (gm/pseudo-motion-interactive-base-in 'kill-region ?\")))

(defun good-mode--kill-dwim (&optional arg)
  "Do the kill operation which seems correct given situation.
The following things are checked, in order:
- If called with a plain prefix argument (C-u), call `good-mode-kill-command'.
This is so you can execute any kill command even using the DWIM approach.
- If region is active, kill it (`kill-region').
- If at the beginning of a defun, kill it (gm/kill-thing \\'defun).
- If at the beginning of a line, kill it (`kill-whole-line').
- If at what seems to be a word, kill it (`gm/kill-whole-word'), unless we
are at emacs-lisp-mode, then kill a sexp instead (`gm/kill-whole-sexp').
- If char before point is a period, kill the previous sentence
(`gm/kill-whole-sentence').
- If at what seems to be a sexp, kill it (`gm/kill-whole-sexp')
- Otherwise, kill until the end of the next word (`kill-word').

Any prefix argument (except by a plain one) is passed to the corresponding
function."
  (interactive "P")
  (let ((bounds (bounds-of-thing-at-point 'word))
        (line-bounds (bounds-of-thing-at-point 'line))
        (sexp-bounds (bounds-of-thing-at-point 'sexp))
        (defun-bounds (bounds-of-thing-at-point 'defun)))
    (if (consp arg)
        (progn
          (setq current-prefix-arg nil)
          (good-mode-kill-command))
      (if (region-active-p)
          (kill-region nil nil t)
        (if (eq (point) (car defun-bounds))
            (gm/kill-thing 'defun arg)
          (if (eq (point) (car line-bounds))
              (kill-whole-line arg)
            (if bounds
                (if (eq major-mode 'emacs-lisp-mode)
                    (gm/kill-whole-sexp arg)
                  (gm/kill-whole-word arg))
              (if (eq (char-before) 46)
                  (gm/kill-whole-sentence arg)
                (if sexp-bounds
                    (gm/kill-whole-sexp arg)
                  (kill-word arg))))))))))

;; Mark functions

(defun gm/mark-sentence (&optional arg)
  "Set mark ARG sentences away from point.
The place mark goes is the same place \\[forward-sentence] would
move to with the same argument.
Interactively, if the mark is active, it marks the next ARG sentences
after the ones already marked."
  (interactive "p")
  (gm/mark-thing 'sentence arg t))

(defun gm/backward-mark-sentence ()
  "Set mark at beginning of sentence."
  (interactive)
  (gm/mark-thing 'sentence -1 t))

(defun gm/mark-whole-sentence (&optional arg)
  "Put point at beginning of sentence, mark at end.
Interactively, if the mark is active, it marks the next ARG sentences
after the ones already marked."
  (interactive "p")
  (gm/mark-thing 'sentence arg))

(defun gm/mark-whole-word (&optional arg)
  "Put point at beginning of word, mark at end.
Interactively, if the mark is active, it marks the next ARG words
after the ones already marked."
  (interactive "p")
  (gm/mark-thing 'word arg))

(defun gm/forward-mark-word (&optional arg)
  "Set mark ARG words away from point looking backwards.
The place mark goes is the same place \\[forward-word] would
move to with the same argument.
Interactively, if the mark is active, it moves the mark forwards."
  (interactive "p")
  (let ((old-point (point)))
       (if (region-active-p)
           (set-mark
            (save-excursion
	      (goto-char (mark))
              (if (< (mark) old-point) (forward-to-word arg) (forward-word arg))
	      (point)))
         (push-mark
	  (save-excursion
	    (forward-word (prefix-numeric-value arg))
	    (point))
	  nil t))))

(defun gm/backward-mark-word (&optional arg)
  "Set mark ARG words away from point looking backwards.
The place mark goes is the same place \\[backward-word] would
move to with the same argument.
Interactively, if the mark is active,
it moves the mark backwards."
  (interactive "p")
  (let ((old-point (point)))
       (if (region-active-p)
           (set-mark
            (save-excursion
	      (goto-char (mark))
              (if (> (mark) old-point) (backward-to-word arg) (backward-word arg))
	      (point)))
         (push-mark
	  (save-excursion
	    (backward-word (prefix-numeric-value arg))
	    (point))
	  nil t))))

(defun gm/mark-line (&optional arg)
  "Set mark ARG lines away from point.
The place mark goes is the same place `forward-line' would
move to with the same argument, unless visual-line-mode is enabled,
then it relies on end-of-visual-line.
Interactively, if the mark is active, it marks the next ARG lines
after the ones already marked."
  (interactive "p")
  (if visual-line-mode
      (cond ((or (and (eq last-command this-command) (mark t))
		 (region-active-p))
	     (setq arg (if arg (prefix-numeric-value arg)
		         (if (< (mark) (point)) -1 1)))
	     (set-mark
	      (save-excursion
	        (goto-char (mark))
	        ;; (forward-thing 'line arg)
                (end-of-visual-line (1+ arg))
	        (point))))
	    (t
	     (push-mark
	      (save-excursion
	        ;; (forward-thing 'line (prefix-numeric-value arg))
                (end-of-visual-line arg)
	        (point))
	      nil t)))
    (gm/mark-thing 'line arg t)))

(defun gm/backward-mark-logical-line ()
  "Put mark at beginning of current logical line."
  (interactive)
  (gm/mark-thing 'line 0 t))

(defun gm/mark-logical-line (&optional arg)
  "Put mark at the end of current logical line.
With ARG, put mark at the end of ARG logical lines."
  (interactive)
  (gm/mark-thing 'line arg t))

(defun gm/mark-whole-line (&optional arg)
  "Put point at beginning of line, mark at end.
Interactively, if the mark is active, it marks the next ARG lines
after the ones already marked."
  (interactive "p")
  (when (eq arg nil) (setq arg 1))
  (if (<= 0 arg)
      (gm/mark-thing 'line arg)
    (progn (gm/mark-thing 'line arg t)
           (unless (gm/change-direction-p arg) (end-of-line)))))

(defun gm/mark-whole-sexp (&optional arg)
  "Put point at beginning of sexp, mark at end.
Interactively, if the mark is active, it marks the next ARG sexps
after the ones already marked."
  (interactive "p")
  (gm/mark-thing 'sexp arg))

(defun gm/cut ()
  "Call `kill-region' and then exit the transient mode."
  (interactive)
  (kill-region nil nil t)
  (good-mode--exit-transient))

(defun gm/kill-ring-save-and-exit ()
  "Call `kill-ring-save' and exit the transient mode."
  (interactive)
  (kill-ring-save nil nil t)
  (good-mode--exit-transient))
  
(defun gm/mark-between-brackets (&optional arg)
  "Put point at first char after a bracket, mark at last.
With ARG, include the brackets."
  (interactive "P")
  (if arg
      (gm/pseudo-motion-interactive-base-an 'gm/mark-region ?\()
    (gm/pseudo-motion-interactive-base-in 'gm/mark-region ?\()))

(defun gm/mark-between-square-brackets (&optional arg)
  "Put point at first char after a square bracket, mark at last.
With ARG, include the square brackets."
  (interactive "P")
  (if arg
      (gm/pseudo-motion-interactive-base-an 'gm/mark-region ?\[)
    (gm/pseudo-motion-interactive-base-in 'gm/mark-region ?\[)))

(defun gm/mark-between-curly-brackets (&optional arg)
  "Put point at first char after a curly bracket, mark at last.
With ARG, include the curly brackets."
  (interactive "P")
  (if arg
      (gm/pseudo-motion-interactive-base-an 'gm/mark-region ?\{)
    (gm/pseudo-motion-interactive-base-in 'gm/mark-region ?\{)))

(defun gm/mark-string (&optional arg)
  "Put point at the first char within a string, mark at last.
With ARG, include the quotes."
  (interactive "P")
  (if arg
      (gm/pseudo-motion-interactive-base-an 'gm/mark-region ?\")
    (gm/pseudo-motion-interactive-base-in 'gm/mark-region ?\")))

(defun good-mode--mark-dwim (&optional arg)
  "Do the mark operation which seems correct given situation.
The following things are checked, in order:
- If called with a plain prefix argument (C-u), call `good-mode-mark-command'.
This is so you can execute any mark command even using the DWIM approach.
- If region is active, save it into kill-ring (`kill-ring-save').
- If at the beginning of a defun, mark it (gm/mark-thing \\'defun).
- If at the beginning of a line, mark it (`gm/mark-whole-line').
- If at what seems to be a word, mark it (`gm/mark-whole-word'), unless we
are at emacs-lisp-mode, then mark a sexp instead (`gm/mark-whole-sexp').
- If char before point is a period, mark the previous sentence
(`gm/mark-whole-sentence').
- If at what seems to be a sexp, mark it (`gm/mark-whole-sexp')
- Otherwise, put mark at the end of the next word (`mark-word').

Any prefix argument (except by a plain one) is passed to the corresponding
function."
  (interactive "P")
  (let ((bounds (bounds-of-thing-at-point 'word))
        (line-bounds (bounds-of-thing-at-point 'line))
        (sexp-bounds (bounds-of-thing-at-point 'sexp))
        (defun-bounds (bounds-of-thing-at-point 'defun)))
    (if (consp arg)
        (progn
          (setq current-prefix-arg nil)
          (good-mode-mark-command))
      (if (region-active-p)
          (kill-ring-save nil nil t)
        (if (eq (point) (car defun-bounds))
            (gm/mark-thing 'defun arg)
          (if (eq (point) (car line-bounds))
              (gm/mark-whole-line arg)
            (if bounds
                (if (eq major-mode 'emacs-lisp-mode)
                    (gm/mark-whole-sexp arg)
                  (gm/mark-whole-word arg))
              (if (eq (char-before) 46)
                  (gm/mark-whole-sentence arg)
                (if sexp-bounds
                    (gm/mark-whole-sexp arg)
                  (mark-word arg))))))))))

;; Keys definition

(defvar good-mode-kill-map
  (let ((map (make-sparse-keymap))
        (universal-argument-minus
         `(menu-item "" negative-argument
                     :filter ,(lambda (cmd)
                                (if (integerp prefix-arg) nil cmd)))))
    (define-key map [?-] universal-argument-minus)
    (define-key map [?0] 'digit-argument)
    (define-key map [?1] 'digit-argument)
    (define-key map [?2] 'digit-argument)
    (define-key map [?3] 'digit-argument)
    (define-key map [?4] 'digit-argument)
    (define-key map [?5] 'digit-argument)
    (define-key map [?6] 'digit-argument)
    (define-key map [?7] 'digit-argument)
    (define-key map [?8] 'digit-argument)
    (define-key map [?9] 'digit-argument)
    (define-key map [kp-0] 'digit-argument)
    (define-key map [kp-1] 'digit-argument)
    (define-key map [kp-2] 'digit-argument)
    (define-key map [kp-3] 'digit-argument)
    (define-key map [kp-4] 'digit-argument)
    (define-key map [kp-5] 'digit-argument)
    (define-key map [kp-6] 'digit-argument)
    (define-key map [kp-7] 'digit-argument)
    (define-key map [kp-8] 'digit-argument)
    (define-key map [kp-9] 'digit-argument)
    (define-key map [?k] 'kill-line)
    (define-key map [?l] 'kill-whole-line)
    (define-key map [?a] 'gm/backward-kill-logical-line)
    (define-key map [?e] 'gm/kill-logical-line)
    (define-key map [?.] 'kill-sentence)        
    (define-key map [?s] 'gm/kill-whole-sentence)
    (define-key map [?p] 'gm/kill-whole-paragraph)
    (define-key map [?X] 'kill-sexp)
    (define-key map [?x] 'gm/kill-whole-sexp)
    (define-key map [?f] 'kill-word)
    (define-key map [?b] 'backward-kill-word)
    (define-key map [?w] 'gm/kill-whole-word)
    (define-key map [?h] 'gm/kill-whole-buffer)
    (define-key map [? ] 'kill-region)
    (define-key map [?o] 'good-mode--kill-dwim)
    (define-key map [?u] 'undo)
    (define-key map [?U] 'undo-redo)
    (define-key map [?z] 'repeat)
    (define-key map [?g] 'good-mode--exit-transient)
    (define-key map [?\(] 'gm/kill-between-brackets)
    (define-key map [?\[] 'gm/kill-between-square-brackets)
    (define-key map [?\{] 'gm/kill-between-curly-brackets)
    (define-key map [?\"] 'gm/kill-string)
    (define-key map [left] 'left-char)
    (define-key map [right] 'right-char)
    (define-key map [up] 'previous-line)
    (define-key map [down] 'next-line)
    (define-key map [kp-subtract] universal-argument-minus)
    map)
  "Keymap used while on \\[good-mode--kill-mode].")

(defvar good-mode-mark-map
  (let ((map (make-sparse-keymap))
        (universal-argument-minus
         `(menu-item "" negative-argument
                     :filter ,(lambda (cmd)
                                (if (integerp prefix-arg) nil cmd)))))
    (define-key map [?-] universal-argument-minus)
    (define-key map [?0] 'digit-argument)
    (define-key map [?1] 'digit-argument)
    (define-key map [?2] 'digit-argument)
    (define-key map [?3] 'digit-argument)
    (define-key map [?4] 'digit-argument)
    (define-key map [?5] 'digit-argument)
    (define-key map [?6] 'digit-argument)
    (define-key map [?7] 'digit-argument)
    (define-key map [?8] 'digit-argument)
    (define-key map [?9] 'digit-argument)
    (define-key map [kp-0] 'digit-argument)
    (define-key map [kp-1] 'digit-argument)
    (define-key map [kp-2] 'digit-argument)
    (define-key map [kp-3] 'digit-argument)
    (define-key map [kp-4] 'digit-argument)
    (define-key map [kp-5] 'digit-argument)
    (define-key map [kp-6] 'digit-argument)
    (define-key map [kp-7] 'digit-argument)
    (define-key map [kp-8] 'digit-argument)
    (define-key map [kp-9] 'digit-argument)
    (define-key map [?k] 'gm/mark-line)
    (define-key map [?l] 'gm/mark-whole-line)
    (define-key map [?a] 'gm/backward-mark-logical-line)
    (define-key map [?e] 'gm/mark-logical-line)
    (define-key map [?.] 'gm/mark-sentence)        
    (define-key map [?s] 'gm/mark-whole-sentence)
    (define-key map [?p] 'mark-paragraph)
    (define-key map [?X] 'mark-sexp)
    (define-key map [?x] 'gm/mark-whole-sexp)
    (define-key map [?f] 'gm/forward-mark-word)
    (define-key map [?b] 'gm/backward-mark-word)
    (define-key map [?w] 'gm/mark-whole-word)
    (define-key map [?h] 'mark-whole-buffer)
    (define-key map [? ] 'gm/kill-ring-save-and-exit)
    (define-key map [?u] 'undo)
    (define-key map [?U] 'undo-redo)
    (define-key map [?t] 'exchange-point-and-mark) 
    (define-key map [?z] 'repeat)
    (define-key map [?g] 'good-mode--exit-transient)
    (define-key map [?c] 'gm/cut)
    (define-key map [left] 'left-char)
    (define-key map [right] 'right-char)
    (define-key map [up] 'previous-line)
    (define-key map [down] 'next-line)
    (define-key map [?o] 'good-mode--mark-dwim)
    (define-key map [?\(] 'gm/mark-between-brackets)
    (define-key map [?\[] 'gm/mark-between-square-brackets)
    (define-key map [?\{] 'gm/mark-between-curly-brackets)
    (define-key map [?\"] 'gm/mark-string)
    (define-key map [kp-subtract] universal-argument-minus)
    map)
  "Keymap used while on \\[good-mode--mark-mode].")

(defun good-mode--exit-transient ()
  "Exit the transient state unconditionally."
  (interactive)
  (setq overriding-terminal-local-map nil)
  (message ""))

(defun good-mode--kill-mode ()
  (prefix-command-update)
  (if (>= emacs-major-version 29)
      (set-transient-map good-mode-kill-map good-mode--use-transient-mode nil good-mode--kill-mode-message good-mode--transient-timeout)
    (progn (set-transient-map good-mode-kill-map good-mode--use-transient-mode 'good-mode--remove-hooks)
           (good-echo good-mode--kill-mode-message)
           (add-hook 'pre-command-hook 'good-mode--echo-kill))))

(defun good-mode-kill-command ()
  "Begin a command for killing text."
  (interactive)
  (prefix-command-preserve-state)
  (good-mode--kill-mode))

(defun good-mode--mark-mode ()
  (prefix-command-update)
  (if (>= emacs-major-version 29)
      (set-transient-map good-mode-mark-map good-mode--use-transient-mode nil good-mode--mark-mode-message good-mode--transient-timeout)
    (progn (set-transient-map good-mode-mark-map good-mode--use-transient-mode 'good-mode--remove-hooks)
           (good-echo good-mode--mark-mode-message)
           (add-hook 'pre-command-hook 'good-mode--echo-mark))))

(defun good-mode-mark-command ()
  "Begin a command for marking text."
  (interactive)
  (prefix-command-preserve-state)
  (good-mode--mark-mode))

(defun good-mode-smart-mark-command (&optional arg)
  "Call good-mode--mark-dwim, then activate mark mode."
  (interactive "p")
  (good-mode--mark-dwim arg)
  (good-mode--mark-mode))

(defvar good-mode--default-map
  (let ((map (make-sparse-keymap)))
    (define-key map (kbd "C-k") #'good-mode-kill-command)
    (define-key map (kbd "M-k") #'good-mode-mark-command)
    map)
  "Keymap used for default \\[good-mode].")

(defvar good-mode--prefix-map
  (let ((map (make-sparse-keymap)))
    ;; Kill commands
    (define-key map (kbd "C-k k")    #'kill-line)
    (define-key map (kbd "C-k l")    #'kill-whole-line)
    (define-key map (kbd "C-k a")    #'gm/backward-kill-logical-line)
    (define-key map (kbd "C-k e")    #'gm/kill-logical-line)
    (define-key map (kbd "C-k .")    #'kill-sentence)
    (define-key map (kbd "C-k DEL")  #'backward-kill-sentence)
    (define-key map (kbd "C-k s")    #'gm/kill-whole-sentence)
    (define-key map (kbd "C-k p")    #'gm/kill-whole-paragraph)
    (define-key map (kbd "C-k X")    #'kill-sexp)
    (define-key map (kbd "C-k x")    #'gm/kill-whole-sexp)
    (define-key map (kbd "C-k f")    #'kill-word)
    (define-key map (kbd "C-k b")    #'backward-kill-word)
    (define-key map (kbd "C-k w")    #'gm/kill-whole-word)
    (define-key map (kbd "C-k h")    #'gm/kill-whole-buffer)
    (define-key map (kbd "C-k SPC")  #'kill-region)
    (define-key map (kbd "C-k o")    #'good-mode--kill-dwim)
    (define-key map (kbd "C-k u")    #'undo)
    (define-key map (kbd "C-k U")    #'undo-redo)
    (define-key map (kbd "C-k z")    #'repeat)
    (define-key map (kbd "C-k (")    #'gm/kill-between-brackets)
    (define-key map (kbd "C-k [")    #'gm/kill-between-square-brackets)
    (define-key map (kbd "C-k {")    #'gm/kill-between-curly-brackets)
    (define-key map (kbd "C-k \"")   #'gm/kill-string)
    ;; For preventing typos
    (define-key map (kbd "C-k C-k")    #'kill-line)
    (define-key map (kbd "C-k C-l")    #'kill-whole-line)
    (define-key map (kbd "C-k C-a")    #'gm/backward-kill-logical-line)
    (define-key map (kbd "C-k C-e")    #'gm/kill-logical-line)
    (define-key map (kbd "C-k C-.")    #'kill-sentence)
    (define-key map (kbd "C-k C-DEL")  #'backward-kill-sentence)
    (define-key map (kbd "C-k C-s")    #'gm/kill-whole-sentence)
    (define-key map (kbd "C-k C-p")    #'gm/kill-whole-paragraph)
    (define-key map (kbd "C-k C-X")    #'kill-sexp)
    (define-key map (kbd "C-k C-x")    #'gm/kill-whole-sexp)
    (define-key map (kbd "C-k C-f")    #'kill-word)
    (define-key map (kbd "C-k C-b")    #'backward-kill-word)
    (define-key map (kbd "C-k C-w")    #'gm/kill-whole-word)
    (define-key map (kbd "C-k C-h")    #'gm/kill-whole-buffer)
    (define-key map (kbd "C-k C-SPC")  #'kill-region)
    (define-key map (kbd "C-k C-o")    #'good-mode--kill-dwim)
    (define-key map (kbd "C-k C-u")    #'undo)
    (define-key map (kbd "C-k C-U")    #'undo-redo)
    (define-key map (kbd "C-k C-z")    #'repeat)
    (define-key map (kbd "C-k C-(")    #'gm/kill-between-brackets)
    (define-key map (kbd "C-k C-[")    #'gm/kill-between-square-brackets)
    (define-key map (kbd "C-k C-{")    #'gm/kill-between-curly-brackets)
    (define-key map (kbd "C-k C-\"")   #'gm/kill-string)
    ;; Mark commands
    (define-key map (kbd "M-k k")    #'gm/mark-line)
    (define-key map (kbd "M-k l")    #'gm/mark-whole-line)
    (define-key map (kbd "M-k a")    #'gm/backward-mark-logical-line)
    (define-key map (kbd "M-k e")    #'gm/mark-logical-line)
    (define-key map (kbd "M-k .")    #'gm/mark-sentence)
    (define-key map (kbd "M-k DEL")  #'gm/backward-mark-sentence)
    (define-key map (kbd "M-k s")    #'gm/mark-whole-sentence)
    (define-key map (kbd "M-k p")    #'mark-paragraph)
    (define-key map (kbd "M-k X")    #'mark-sexp)
    (define-key map (kbd "M-k x")    #'gm/mark-whole-sexp)
    (define-key map (kbd "M-k f")    #'gm/forward-mark-word)
    (define-key map (kbd "M-k b")    #'gm/backward-mark-word)
    (define-key map (kbd "M-k w")    #'gm/mark-whole-word)
    (define-key map (kbd "M-k h")    #'mark-whole-buffer)
    (define-key map (kbd "M-k SPC")  #'kill-ring-save)
    (define-key map (kbd "M-k o")    #'good-mode--mark-dwim)
    (define-key map (kbd "M-k u")    #'undo)
    (define-key map (kbd "M-k U")    #'undo-redo)
    (define-key map (kbd "M-k z")    #'repeat)
    (define-key map (kbd "M-k (")    #'gm/mark-between-brackets)
    (define-key map (kbd "M-k [")    #'gm/mark-between-square-brackets)
    (define-key map (kbd "M-k {")    #'gm/mark-between-curly-brackets)
    (define-key map (kbd "M-k \"")   #'gm/mark-string)
    ;; For preventing typos
    (define-key map (kbd "M-k M-k")    #'gm/mark-line)
    (define-key map (kbd "M-k M-l")    #'gm/mark-whole-line)
    (define-key map (kbd "M-k M-a")    #'gm/backward-mark-logical-line)
    (define-key map (kbd "M-k M-e")    #'gm/mark-logical-line)
    (define-key map (kbd "M-k M-.")    #'gm/mark-sentence)
    (define-key map (kbd "M-k M-DEL")  #'gm/backward-mark-sentence)
    (define-key map (kbd "M-k M-s")    #'gm/mark-whole-sentence)
    (define-key map (kbd "M-k M-p")    #'mark-paragraph)
    (define-key map (kbd "M-k M-X")    #'mark-sexp)
    (define-key map (kbd "M-k M-x")    #'gm/mark-whole-sexp)
    (define-key map (kbd "M-k M-f")    #'gm/forward-mark-word)
    (define-key map (kbd "M-k M-b")    #'gm/backward-mark-word)
    (define-key map (kbd "M-k M-w")    #'gm/mark-whole-word)
    (define-key map (kbd "M-k M-h")    #'mark-whole-buffer)
    (define-key map (kbd "M-k M-SPC")  #'kill-ring-save)
    (define-key map (kbd "M-k M-o")    #'good-mode--mark-dwim)
    (define-key map (kbd "M-k M-u")    #'undo)
    (define-key map (kbd "M-k M-U")    #'undo-redo)
    (define-key map (kbd "M-k M-z")    #'repeat)
    (define-key map (kbd "M-k M-(")    #'gm/mark-between-brackets)
    (define-key map (kbd "M-k M-[")    #'gm/mark-between-square-brackets)
    (define-key map (kbd "M-k M-{")    #'gm/mark-between-curly-brackets)
    (define-key map (kbd "M-k M-\"")   #'gm/mark-string)
    map)
  "Keymap used for \\[good-mode] when using prefix approach.")

(defvar good-mode--dwim-map
  (let ((map (make-sparse-keymap)))
    (define-key map (kbd "C-k") #'good-mode--kill-dwim)
    (define-key map (kbd "M-k") #'good-mode--mark-dwim)
    map)
  "Keymap used for \\[good-mode] when using DWIM approach.")

;;;###autoload
(define-minor-mode good-mode
  "Provides simple and advanced commands for killing or marking text.
When you toggle good-mode, it enables two transient modes which you can use
for accesing functions that kill and mark text.
By default, you can activate them with C-k and M-k."
  :require 'good-mode
  :keymap good-mode--default-map
  :lighter nil
  :global t
  (when good-mode--dwim-mode (good-mode--dwim-mode -1))
  (when good-mode--prefix-mode (good-mode--prefix-mode -1)))

;;;###autoload
(define-minor-mode good-mode--prefix-mode
  "Provides simple and advanced commands for killing or marking text.
When you toggle good-mode--prefix-mode, it enables two prefix commands
which you can use for accesing functions that kill and mark text.
By default, these prefix are C-k and M-k."
  :require 'good-mode
  :keymap good-mode--prefix-map
  :lighter nil
  :global t
  (when good-mode--dwim-mode (good-mode--dwim-mode -1))
  (when good-mode (good-mode -1)))

;;;###autoload
(define-minor-mode good-mode--dwim-mode
  "Provides simple and advanced commands for killing or marking text.
When you toggle good-mode--prefix-mode, it enables two commands
which you can use for accesing functions that kill and mark text.
By default, these prefix are C-k and M-k."
  :require 'good-mode
  :keymap good-mode--dwim-map
  :lighter nil
  :global t
  (when good-mode--prefix-mode (good-mode--prefix-mode -1))
  (when good-mode (good-mode -1)))

(provide 'good-mode)

;;; good-mode.el ends here
